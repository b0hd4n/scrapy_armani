import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import datetime
import re
import string

class MySpider(CrawlSpider):
    name = 'armani'
    allowed_domains = ['armani.com']
    user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36"
    rules = (
        # parse item page
        Rule(LinkExtractor(allow=('cod.*html', )), callback='parse_item'),
        # go through the store pages
        Rule(LinkExtractor(
            allow=('www.armani.com/[a-zA-Z]{2}/'), 
            deny=('=', '\?', 'storelocator')),
        ),
    )

    def start_requests(self):
        if hasattr(self, 'regions'):
            regions = [r for r in self.regions.split(',') if r.isalpha() and len(r) == 2]
        else:
            regions = ['us','fr']
        for region in regions:
            yield scrapy.Request('http://www.armani.com/%s/' % region)

    def get_availability(self, response):
        a = response.css('script').extract_first()
        b = a[a.find('jsoninit_availability'):]
        c = b[:b.find('};')+1]
        self.logger.debug(c)
        color = re.search('Color":"(\w{1,10})', c).group(1)
        color_rgb = re.search('Rgb":"(.{6})', c).group(1)
        sizes = re.search('SizeW":\[([^\]]*)\],"Si', c).group(1).split(',')
        return {'color': color, 'color_rgb': color_rgb, 'sizes': sizes}

    def parse_item(self, response):
        self.logger.debug('Hi, this is an item page! %s', response.url)
        
        timestamp = datetime.datetime.now()
        item = {
            'name': response.css('h1.productName::text').extract_first(),
            'price': response.css('span.priceValue::text').extract_first(),
            'currency': response.css('span.currency::text').extract_first(),
            'category': string.join(response.css('li.selected > a::text').extract(), "|"),
            'timestamp': timestamp,
            'region': response.url.split('/')[3],
            'description': response.css('div.descriptionContent::text').extract_first(),
        }
        availability = self.get_availability(response)
        item['color'] = availability['color']
        item['color_rgb'] = availability['color_rgb']
        for size in availability['sizes']:
            item['size'] = size
            yield item
