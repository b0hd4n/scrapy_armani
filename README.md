# Armani crawler

run (by default for us and fr regions):
	$ scrapy crawl armani

use comma separated two-letter country codes to specify regions:
	$ scrapy crawl armani -a regions=ua,uk


## Checking results with Pandas ##
run & check:
$ python run.py

just check already received results:
$ python process_csv_result.py result.csv